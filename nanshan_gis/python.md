# python部分

&emsp;&emsp;接下來的兩個章節在嘗試用python的程式碼來呈現我們在QGIS部分做過的交角計算以及一些延伸的統計分析。至於為甚麼已經可以用QGIS完成的事情還需要用python再做一次呢？主要的因素在於用QGIS進行分析是一系列的步驟與流程，而全部用物件導向的軟體計算的壞處，就是無法一次運行以後直接生成想要的結果。若用程式來表達的話就可以達到自動化的效果、降低後續的使用門檻，雖然若可以做到直接變成一個執行檔`.exe`是最理想的狀態，但依我目前的能力，能產出一個可執行的jupyter notebook可能已經是可以達到的極限。

&emsp;&emsp;另外一個部分是我們會做一些簡單的數據分析，以及生成一些統計圖表。主要是用簡單的線性迴歸分析（linear regression analysis）進行，來看某一些統計出來的參數是否有相關性。
