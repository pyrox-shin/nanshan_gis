# 以地理資訊系統進行南山公墓點位與環境的關聯性分析
Analysis of relationship between the tomb locations and physical environment in Nanshan Cemetry by GIS

## Introduction

This repository stores a Pages about my project at Center of GIS, RCHSS, Academia Sinica. My project is about how to preserve and present the **cultural landscape**  with the concept of **power mapping** in Nanshan Cemetry by GIS and python. There are a lot of Chinese-styled tombs in the cemetry that follow the Feng Shui (風水) convention that is highly depends on surronding environment, and my job is to use **GIS** to prove the cultural phenomenon.

After all, I found most of the tomb follow the rule, but some of them are not. Besides some reserch limitations, as a cultural researcher, I think it's important to discuss the heterogeneity in all these tombs. The main goal is not to prove all of the tomb follow the Feng Shui convention, but why some of them aren't. Hence, the conclusion will launch a new journey and bring us to find more details in the process of "becoming" landscape.

This project is written by Chinese currently. It may will have a English version in the near future.

## Dependencies

The contents included **jupyter notebook** that use ipython kernal as language, you may need a environment to run it. Or, you can use **binder** to run the codes.

## Usage

The mapping process is already automated by python code. As long as you follow the format of sheet and project it to geocoded format (**shapefile** specifically), the code will produce the map to imaage (png) and html (folium map) for you.

## Acknowledgement

This project is accomplished under the guidance of [Prof. Chuang, Tyng-Ruey](https://homepage.iis.sinica.edu.tw/pages/trc/index_zh.html) @ IIS, Academia Sinica and all the members in [depositar lab](https://lab.depositar.io/members/). I sincerely appreciate their help and teaching. I also want to thank all the intern in both depositar lab and center for GIS, the project couldn't be done without your helps and accompany.  